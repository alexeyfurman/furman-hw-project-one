// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <GameFramework/CharacterMovementComponent.h>
#include <Components/PrimitiveComponent.h>
#include <Components/SphereComponent.h>
#include "SpeedBoostCPP.generated.h"

class UStaticMeshComponent;

UCLASS()

class MYPROJECTLC2_API ASpeedBoostCPP : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpeedBoostCPP();

	bool GetGenerateOverlapEvents() const;

	bool SetNotifyRigidBodyCollision();

	UPROPERTY()
	FComponentBeginOverlapSignature OnComponentBeginOverlap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* SphereVisualComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

public:	
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};