// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBoostCPP.h"
#include <ConstructorHelpers.h>


// Sets default values
ASpeedBoostCPP::ASpeedBoostCPP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));
	if (SphereMeshAsset.Succeeded())
	{
			SphereVisualComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SphereVisualComponent"));
			SphereVisualComponent->SetStaticMesh(SphereMeshAsset.Object);
			SphereVisualComponent->BodyInstance.SetCollisionProfileName(TEXT("OverlapAll"));
	}

	SphereVisualComponent->OnComponentBeginOverlap.AddDynamic(this, &ASpeedBoostCPP::OnOverlapBegin);

}

bool ASpeedBoostCPP::GetGenerateOverlapEvents() const
{
	return true;
}

bool ASpeedBoostCPP::SetNotifyRigidBodyCollision()
{
	return true;
}

// Called when the game starts or when spawned
void ASpeedBoostCPP::BeginPlay()
{

}

void ASpeedBoostCPP::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Destroy();
	UE_LOG(LogTemp, Warning, TEXT("Activated"));
}

// Called every frame
void ASpeedBoostCPP::Tick(float DeltaTime)
{
	
}
