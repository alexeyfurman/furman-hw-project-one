// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MyCPPACharacter.h"
#include "BaseWeapon.generated.h"


/**
 * 
 */
UCLASS()
class MYPROJECTLC2_API ABaseWeapon : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	ABaseWeapon();

	virtual void StartFire();

	virtual void StopFire();

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Muzzle")
	FName MuzzleSocketName;

	UFUNCTION()
	virtual void Fire();
	
	UPROPERTY()
		ACharacter* WeaponOwner;

	//UPROPERTY()
		//class UAnimMontage* Fire_Rifle_Hip_Montage;

	FTransform GetMuzzleTransform();

	virtual void BeginPlay() override;

};
