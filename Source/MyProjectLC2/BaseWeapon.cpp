// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseWeapon.h"
#include "MyCPPACharacter.h"
#include <MessageDialog.h>

ABaseWeapon::ABaseWeapon()
{
	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);
}

void ABaseWeapon::StartFire()
{

	Fire();

}

void ABaseWeapon::StopFire()
{

}

void ABaseWeapon::Fire()
{
	//if (Fire_Rifle_Hip_Montage)
	//{
	//	if (WeaponOwner)
	//	{
	//		WeaponOwner->PlayAnimMontage(Fire_Rifle_Hip_Montage);
//
	//	}
	//	else
	//	{
	//		UE_LOG(LogTemp, Warning, TEXT("No Owner Found"));
	//	}
//
	//}
	//else
	//{
//		UE_LOG(LogTemp, Warning, TEXT("No Fire Montage found"))
//	}
}

FTransform ABaseWeapon::GetMuzzleTransform()
{	
	if (MuzzleSocketName == "None")
	{
		FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "Muzzle Socket Name Not Initialized"));
		return FTransform::Identity;
	}
	
	return GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
}

void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	
	GetMuzzleTransform();

	//if (GetOwner())
	//{
	//	WeaponOwner = Cast<ACharacter>(GetOwner());
	//}
}

