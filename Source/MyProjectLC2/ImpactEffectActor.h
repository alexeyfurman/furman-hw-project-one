// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ImpactEffectActor.generated.h"

UCLASS()
class MYPROJECTLC2_API AImpactEffectActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AImpactEffectActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void HitInit(FHitResult);

protected:
	// Called when the game starts or when spawned

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UMaterialInterface* DecalMaterial;

	FHitResult EffectHit;
	
	void SpawnEffects();

	void HitInit();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
