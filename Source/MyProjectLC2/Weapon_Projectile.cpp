// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_Projectile.h"
#include "BaseProjectile.h"

void AWeapon_Projectile::Fire()
{

	Super::Fire();

	//Spawn Projectile
	if (ProjectileClass)
	{
		FTransform TmpTransform = GetMuzzleTransform();

		GetWorld()->SpawnActor(ProjectileClass, &TmpTransform);

	}


}
