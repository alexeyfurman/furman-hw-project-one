// Fill out your copyright notice in the Description page of Project Settings.


#include "ImpactEffectActor.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AImpactEffectActor::AImpactEffectActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AImpactEffectActor::BeginPlay()
{
	Super::BeginPlay();

	SpawnEffects();
	a
}

void AImpactEffectActor::HitInit(FHitResult Hit)
{
	EffectHit = Hit;
}

void AImpactEffectActor::SpawnEffects()
{
	if (DecalMaterial)
	{
		FRotator DecalRotation = FRotationMatrix::MakeFromX(EffectHit.ImpactNormal).Rotator();

		float RandomRoation = FMath::FRandRange(-180, 180);
		UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(4, 8, 8), EffectHit.GetComponent(), EffectHit.BoneName, EffectHit.ImpactPoint, DecalRotation, EAttachLocation::KeepWorldPosition, 5);
	}
}

// Called every frame
void AImpactEffectActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

