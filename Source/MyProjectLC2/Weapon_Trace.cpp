// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_Trace.h"

// Sets default values
AWeapon_Trace::AWeapon_Trace()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWeapon_Trace::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeapon_Trace::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

