// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCPPACharacter.h"
#include <Components/InputComponent.h>
#include <GameFramework/SpringArmComponent.h>
#include <Camera/CameraComponent.h>
#include <ConstructorHelpers.h>
#include <RotationMatrix.h>
#include <UnrealMath.h>
#include <BaseWeapon.h>



// Sets default values
AMyCPPACharacter::AMyCPPACharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComponent = CreateAbstractDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateAbstractDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	
	//Setting Character's Mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkelMesh(TEXT("SkeletalMesh'/Game/AnimStarterPack/UE4_Mannequin/Mesh/SK_Mannequin.SK_Mannequin'"));

	if (SkelMesh.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SkelMesh.Object);
	}
	
	//Rotating Mesh
	GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));

	bUseControllerRotationYaw = false;

	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bInheritYaw = false;

	// Setting Animation Blueprint
	static ConstructorHelpers::FObjectFinder<UAnimBlueprint> AnimationBP(TEXT("AnimBlueprint'/Game/AnimStarterPack/UE4_Mannequin/Mesh/ABP_MyCharacter.ABP_MyCharacter'"));
	GetMesh()->SetAnimInstanceClass(AnimationBP.Object->GetAnimBlueprintGeneratedClass());

	static ConstructorHelpers::FObjectFinder<UAnimMontage> WeaponFire(TEXT("AnimMontage'/Game/AnimStarterPack/Fire_Rifle_Hip_Montage.Fire_Rifle_Hip_Montage'"));
	if (WeaponFire.Succeeded())
	{
		Fire_Rifle_Hip_Montage = WeaponFire.Object;
	}


	static ConstructorHelpers::FObjectFinder<UAnimMontage> WeaponReload(TEXT("AnimMontage'/Game/AnimStarterPack/Reload_Rifle_Hip_Montage.Reload_Rifle_Hip_Montage'"));
	if (WeaponReload.Succeeded())
	{
		Reload_Rifle_Hip_Montage = WeaponReload.Object;
	}

}

// Called when the game starts or when spawned
void AMyCPPACharacter::BeginPlay()
{
	Super::BeginPlay();

	CurrentWeapon = SpawnWeapon();
	AttachWeapon(WeaponSocket);
	
}

ABaseWeapon * AMyCPPACharacter::SpawnWeapon()
{
	if (WeaponToSpawn_Class)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		UE_LOG(LogTemp, Warning, TEXT("Trying to spawn weapon"));
		return Cast<ABaseWeapon>(GetWorld()->SpawnActor(WeaponToSpawn_Class, &TmpTransform));
	}

	return nullptr;
}

void AMyCPPACharacter::AttachWeapon(FName SocketName)
{
	if (CurrentWeapon)
	{	
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
		//Fire_Rifle_Hip_Montage = CurrentWeapon->Fire_Rifle_Hip_Montage;
	}
}

// Called every frame
void AMyCPPACharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RotateMouse();

}

void AMyCPPACharacter::RotateMouse()
{
	FHitResult Hit;

	if (GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit))
	{
		float RotationYaw = FRotationMatrix::MakeFromX(Hit.ImpactPoint - GetActorLocation()).Rotator().Yaw;
		SetActorRotation(FRotator(0, RotationYaw, 0));
	}

}

// Called to bind functionality to input
void AMyCPPACharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMyCPPACharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AMyCPPACharacter::MoveRight);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &AMyCPPACharacter::StartSprint);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &AMyCPPACharacter::StopSprint);
	PlayerInputComponent->BindAxis(TEXT("CameraMove"), this, &AMyCPPACharacter::CameraMove);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Pressed, this, &AMyCPPACharacter::StartWalk);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &AMyCPPACharacter::StopWalk);
	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &AMyCPPACharacter::Reload);
	PlayerInputComponent->BindAction(TEXT("Hide/Take Weapon"), IE_Pressed, this, &AMyCPPACharacter::Equip);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AMyCPPACharacter::StartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AMyCPPACharacter::StopFire);

}

void AMyCPPACharacter::MoveForward(float AxisValue)
{
	AddMovementInput(FVector(1, 0, 0), AxisValue);
}

void AMyCPPACharacter::MoveRight(float AxisValue)
{
	AddMovementInput(FVector (0, 1, 0), AxisValue);
}

void AMyCPPACharacter::StartSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
}

void AMyCPPACharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = JogSpeed;
}

void AMyCPPACharacter::StartWalk()
{
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
}

void AMyCPPACharacter::StopWalk()
{
	GetCharacterMovement()->MaxWalkSpeed = JogSpeed;
}

void AMyCPPACharacter::Fire()
{
	USkeletalMeshComponent* Mesh = GetMesh();
	if (Mesh)
	{
		UAnimInstance* AnimInst = Mesh->GetAnimInstance();
		if (AnimInst)
		{
			AnimInst->Montage_Play(Fire_Rifle_Hip_Montage);
		}
	}
}

void AMyCPPACharacter::Reload()
{
	GetMesh()->GetAnimInstance()->Montage_Play(Reload_Rifle_Hip_Montage);
}

void AMyCPPACharacter::Equip()
{
}

void AMyCPPACharacter::CameraMove(float AxisValue)
{
	CamZoomDestination = AxisValue * 50 + FMath::Clamp(CamZoomDestination, 800.f, 1200.f);

	SpringArmComponent->TargetArmLength = FMath::FInterpTo(SpringArmComponent->TargetArmLength, CamZoomDestination, GetWorld()->DeltaTimeSeconds, 5.0f);
}

void AMyCPPACharacter::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
}
}

void AMyCPPACharacter::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}


